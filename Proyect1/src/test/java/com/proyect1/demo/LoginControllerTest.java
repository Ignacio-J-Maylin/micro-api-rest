package com.proyect1.demo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import com.proyect1.demo.request.LoginRequest;
import com.proyect1.demo.request.NewClientRequest;
import com.proyect1.demo.request.NewManagerAdminRequest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:db-test.properties")
@Sql("/test-mysql.sql")
public class LoginControllerTest {
	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	int randomServerPort;

	
//////////////////////////////////// START TEST CREATE CLIENT//////////////////////////////////////////////////////
	@Test
	public void testAddClientSuccess() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/client";
		URI uri = new URI(baseUrl);
		NewClientRequest newClient = new NewClientRequest("nuevocliente1@gmail.com", "nuevoCliente1", "Nuevo", "Cliente",
				"nuevoCliente1", 1500000000, LocalDate.of(2015, 01, 01), "nuevoCliente 1", "4A", "La Ferrere",
				"Buenos Aries", "Argentina", "1566b");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewClientRequest> request = new HttpEntity<>(newClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request succeed
		Assert.assertEquals(201, result.getStatusCodeValue());
		System.out.println(result.getBody());
	}

	@Test
	public void testAddClientBadRequestEmailIsInUse() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/client";
		URI uri = new URI(baseUrl);
		NewClientRequest newClient = new NewClientRequest("ignaciomaylin@gmail.com", "nuevoCliente1", "Nuevo", "Cliente",
				"nuevoCliente1", 1500000000, LocalDate.of(2015, 01, 01), "nuevoCliente 1", "4A", "La Ferrere",
				"Buenos Aries", "Argentina", "1566b");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewClientRequest> request = new HttpEntity<>(newClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(412, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}

	@Test
	public void testAddClientBadRequestNickIsInUse() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/client";
		URI uri = new URI(baseUrl);
		NewClientRequest newClient = new NewClientRequest("nuevoCliente@gmail.com", "nachito", "Nuevo", "Cliente", "nuevoCliente1",
				1500000000, LocalDate.of(2015, 01, 01), "nuevoCliente 1", "4A", "La Ferrere", "Buenos Aries",
				"Argentina", "1566b");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewClientRequest> request = new HttpEntity<>(newClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(412, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}

	@Test
	public void testAddClientBadRequestDateOfBirthMustBeInThePast() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/client";
		URI uri = new URI(baseUrl);
		NewClientRequest newClient = new NewClientRequest("nuevoCliente@gmail.com", "nachito", "Nuevo", "Cliente", "nuevoCliente1",
				1500000000, LocalDate.of(2025, 01, 01), "nuevoCliente 1", "4A", "La Ferrere", "Buenos Aries",
				"Argentina", "1566b");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewClientRequest> request = new HttpEntity<>(newClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(417, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}
	
	@Test
	public void testAddClientBadRequestPasswordMustBeAtLeast8CaractersLength() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/client";
		URI uri = new URI(baseUrl);
		NewClientRequest newClient = new NewClientRequest("nuevoCliente@gmail.com", "nachito", "Nuevo", "Cliente", "abcd123",
				1500000000, LocalDate.of(2015, 01, 01), "nuevoCliente 1", "4A", "La Ferrere", "Buenos Aries",
				"Argentina", "1566b");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewClientRequest> request = new HttpEntity<>(newClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(417, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}
	@Test
	public void testAddClientBadRequestPasswordWithOutLeters() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/client";
		URI uri = new URI(baseUrl);
		NewClientRequest newClient = new NewClientRequest("nuevoCliente@gmail.com", "nachito", "Nuevo", "Cliente", "12345678",
				1500000000, LocalDate.of(2015, 01, 01), "nuevoCliente 1", "4A", "La Ferrere", "Buenos Aries",
				"Argentina", "1566b");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewClientRequest> request = new HttpEntity<>(newClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(417, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}
	@Test
	public void testAddClientBadRequestPasswordWithOutNumbers() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/client";
		URI uri = new URI(baseUrl);
		NewClientRequest newClient = new NewClientRequest("nuevoCliente@gmail.com", "nachito", "Nuevo", "Cliente", "abcdefgh",
				1500000000, LocalDate.of(2015, 01, 01), "nuevoCliente 1", "4A", "La Ferrere", "Buenos Aries",
				"Argentina", "1566b");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewClientRequest> request = new HttpEntity<>(newClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(417, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}
	

////////////////////////////////////END TEST CREATE CLIENT//////////////////////////////////////////////////////

	
////////////////////////////////////START TEST CREATE ADMIN//////////////////////////////////////////////////////
	
	@Test
	public void testAddManagerAdminSuccess() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/manager/admin";
		URI uri = new URI(baseUrl);
		NewManagerAdminRequest newManagerAdminAdmin = new NewManagerAdminRequest("admin2@gmail.com", "admin2", "admin", "admin",
				"admin1234", "Qwerty123456");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewManagerAdminRequest> request = new HttpEntity<>(newManagerAdminAdmin, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request succeed
		Assert.assertEquals(201, result.getStatusCodeValue());
		System.out.println(result.getBody());
	}

	@Test
	public void testAddManagerAdminBadRequestEmailIsInUse() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/manager/admin";
		URI uri = new URI(baseUrl);
		NewManagerAdminRequest newManagerAdminAdmin = new NewManagerAdminRequest("ignaciomaylin@gmail.com", "admin1", "admin", "admin",
				"admin1234", "Qwerty123456");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewManagerAdminRequest> request = new HttpEntity<>(newManagerAdminAdmin, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(412, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}

	@Test
	public void testAddManagerAdminBadRequestNickIsInUse() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/manager/admin";
		URI uri = new URI(baseUrl);
		NewManagerAdminRequest newManagerAdminAdmin = new NewManagerAdminRequest("admin1@gmail.com", "nachito", "admin", "admin",
				"admin1234", "Qwerty123456");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewManagerAdminRequest> request = new HttpEntity<>(newManagerAdminAdmin, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(412, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}

	
	
	@Test
	public void testAddManagerAdminRequestPasswordMustBeAtLeast8CaractersLength() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/manager/admin";
		URI uri = new URI(baseUrl);
		NewManagerAdminRequest newManagerAdminAdmin = new NewManagerAdminRequest("admin1@gmail.com", "admin1", "admin", "admin",
				"admin12", "Qwerty123456");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewManagerAdminRequest> request = new HttpEntity<>(newManagerAdminAdmin, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(417, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}
	@Test
	public void testAddManagerAdminBadRequestPasswordWithOutLeters() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/manager/admin";
		URI uri = new URI(baseUrl);
		NewManagerAdminRequest newManagerAdminAdmin = new NewManagerAdminRequest("admin1@gmail.com", "admin1", "admin", "admin",
				"12345678", "Qwerty123456");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewManagerAdminRequest> request = new HttpEntity<>(newManagerAdminAdmin, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(417, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}
	@Test
	public void testAddManagerAdminBadRequestPasswordWithOutNumbers() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/manager/admin";
		URI uri = new URI(baseUrl);
		NewManagerAdminRequest newManagerAdminAdmin = new NewManagerAdminRequest("admin1@gmail.com", "admin1", "admin", "admin",
				"abcdefgh", "Qwerty123456");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewManagerAdminRequest> request = new HttpEntity<>(newManagerAdminAdmin, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(417, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}

	@Test
	public void testAddManagerAdminBadRequestValidCodeIsInvalid() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login/singUp/manager/admin";
		URI uri = new URI(baseUrl);
		NewManagerAdminRequest newManagerAdminAdmin = new NewManagerAdminRequest("admin1@gmail.com", "admin1", "admin", "admin",
				"abcdefgh", "qwerty123456");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<NewManagerAdminRequest> request = new HttpEntity<>(newManagerAdminAdmin, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request badRequest
		Assert.assertEquals(417, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}
	

////////////////////////////////////STOP TEST CREATE ADMIN//////////////////////////////////////////////////////
////////////////////////////////////START TEST LOGIN ADMIN//////////////////////////////////////////////////////




	@Test
	public void testLoginClientSuccess() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login";
		URI uri = new URI(baseUrl);
		LoginRequest loginClient = new LoginRequest("nachito", "nuevoCliente1");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<LoginRequest> request = new HttpEntity<>(loginClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request success
		Assert.assertEquals(200, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}
	
	
	@Test
	public void testLoginManagerSuccess() throws URISyntaxException {
		final String baseUrl = "http://localhost:" + randomServerPort + "/login";
		URI uri = new URI(baseUrl);
		LoginRequest loginClient = new LoginRequest("admin1", "admin1234");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<LoginRequest> request = new HttpEntity<>(loginClient, headers);
		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		// Verify request success
		Assert.assertEquals(200, result.getStatusCodeValue());
		System.out.println(result.getBody().toString());
	}





}