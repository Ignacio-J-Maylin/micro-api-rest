BEGIN;
SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE  `invoice_detail`;
TRUNCATE  `general_invoice`;
TRUNCATE  `address`;
TRUNCATE  `client`;
TRUNCATE  `manager`;
TRUNCATE  `roles`;
TRUNCATE  `user`;
TRUNCATE  `product`;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `user` (`nick`, `created_date`, `down_date`, `email`, `first_name`, `last_name`,`password`) VALUES
    ("nachito","2020-08-02 17:37:42", null, "ignaciomaylin@gmail.com","Ignacio", "Maylin","$2a$10$h5H0.m/bSYuGmERDMP7yPuOgLzH/pN9wZf4HtZI7QrVkW2x/SokEu"),
    ("jesica","2019-08-02 17:37:42", null, "jesicamanoukian@gmail.com","Jesica", "Manoukian","$2a$10$h5H0.m/bSYuGmERDMP7yPuOgLzH/pN9wZf4HtZI7QrVkW2x/SokEu"),
    ("wity","2018-08-02 17:37:42", null, "witymartines@gmail.com","Javier", "Martines","$2a$10$h5H0.m/bSYuGmERDMP7yPuOgLzH/pN9wZf4HtZI7QrVkW2x/SokEu"),
    ("admin1","2018-08-02 17:37:42", null, "admin1@gmail.com","admin", "admin","$2a$10$K6M4EoDVhEV0MCgTb/FbPOQRWz7L1S66m0B0RJf6lKjCMnefDeXAW");


INSERT INTO `address` (`address_id`, `city`, `province`, `country`, `department`, `postal_code`, `street_and_height`,`user_nick`, `created_date`, `down_date`, `modified_date`) VALUES
    (1,"Burzaco", "Buenos Aires", "Argentina","home", "1558b","Florencio Parravichini 282","nachito","2020-08-02 17:37:42",null,null),
    (2,  "Burzaco", "Buenos Aires", "Argentina","home", "1558b","Florencio Parravichini 282","jesica","2019-08-02 17:37:42",null,null),
    (3,  "Burzaco", "Buenos Aires", "Argentina","home", "1558b","Florencio Parravichini 282","wity","2018-08-02 17:37:42",null,null);

INSERT INTO `client` (`date_of_birth`, `mobile`, `points`, `nick`, `address_id`) VALUES
    ("2020-08-02 17:37:42",  1500000001, 0, "nachito",1),
    ("2019-08-02 17:37:42",  1500000002, 0, "jesica",2),
    ("2018-08-02 17:37:42",  1500000003, 0, "wity",3);

INSERT INTO `manager` (`base_salary`, `nick`) VALUES
    (3000,"admin1");


    INSERT INTO `roles` (`roles_id`, `role`, `nick`, `avaible`, `created_date`, `modified_date`) VALUES
    (1, "ROLE_CLIENT", "nachito",1,"2020-08-02 17:37:42",null),
    (2, "ROLE_CLIENT", "jesica",1,"2019-08-02 17:37:42",null),
    (3, "ROLE_CLIENT", "wity",1,"2018-08-02 17:37:42",null),
    (4, "ROLE_MANAGER", "admin1",1,"2018-08-02 17:37:42",null),
    (5, "ROLE_SELLER", "admin1",1,"2018-08-02 17:37:42",null),
    (6, "ROLE_LOGISTIC", "admin1",1,"2018-08-02 17:37:42",null),
    (7, "ROLE_CHEFF", "admin1",1,"2018-08-02 17:37:42",null),
    (8, "ROLE_ADMIN", "admin1",1,"2018-08-02 17:37:42",null),
    (9, "ROLE_ACCOUNTANT", "admin1",1,"2018-08-02 17:37:42",null);

COMMIT;