package com.proyect1.demo.enums;

public enum OrderStatus {
	OBSERVATION,REGISTERED,PREPARING,ON_WAY,DELIVERED,CANCELED
}
