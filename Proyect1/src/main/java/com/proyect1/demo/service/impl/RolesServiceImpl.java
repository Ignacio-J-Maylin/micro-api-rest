package com.proyect1.demo.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyect1.demo.entity.Client;
import com.proyect1.demo.entity.Manager;
import com.proyect1.demo.entity.Roles;
import com.proyect1.demo.enums.Role;
import com.proyect1.demo.repository.RolesRepository;
import com.proyect1.demo.service.RolesService;

@Service
public class RolesServiceImpl implements RolesService {

	@Autowired
	private RolesRepository rolesRepo;

	@Override
	public List<Roles> addNewRollClient(Client clientCreated) {
		List<Roles> roles = new ArrayList<Roles>();
		roles.add(rolesRepo.save(new Roles(clientCreated, Role.ROLE_CLIENT)));
		return roles;
	}

	@Override
	public void addNewRollManager(Manager managerCreated) {
		List<Roles> roles = managerCreated.getRoles();
		rolesRepo.saveAll(roles);
	}

	

	@Override
	public void addNewRoles(Manager manager, Role newRole) {
		List<Roles> roles = new ArrayList<Roles>();
		roles.add(rolesRepo.save(new Roles(manager, newRole)));
	}

	@Override
	public void upgrade(Roles oldRoles, boolean contains) {
		if (contains) {
			oldRoles.setAvailable(true);
			oldRoles.setModifiedDate(LocalDateTime.now());
		} else {
			oldRoles.setAvailable(false);
			oldRoles.setModifiedDate(LocalDateTime.now());

		}
		rolesRepo.save(oldRoles);
	}

	@Override
	public void activate(Roles oldRole) {
		oldRole.setAvailable(true);
		oldRole.setModifiedDate(LocalDateTime.now());
		rolesRepo.save(oldRole);
	}
	
	@Override
	public void inactivate(Roles oldRole) {
		oldRole.setAvailable(false);
		oldRole.setModifiedDate(LocalDateTime.now());
		rolesRepo.save(oldRole);
	}

}
