package com.proyect1.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyect1.demo.entity.Manager;
import com.proyect1.demo.entity.Roles;
import com.proyect1.demo.enums.Role;
import com.proyect1.demo.repository.ManagerRepository;
import com.proyect1.demo.request.NewManagerRequest;
import com.proyect1.demo.security.PasswordEncoderService;
import com.proyect1.demo.service.AdminService;
import com.proyect1.demo.service.RolesService;

@Service
public class AdminServiceImpl implements AdminService {
	

	@Autowired
	private PasswordEncoderService passwordEncoderService;
	@Autowired
	private RolesService rolesService;
	@Autowired
	private ManagerRepository managerRepo;

	@Override
	@Transactional
	public Manager createManager(@Valid NewManagerRequest newManager, Role role) {
		List<Role> listRole= new ArrayList<Role>();
		listRole.add(role);
		Manager manager = new Manager(newManager.getEmail(), newManager.getNick(), newManager.getFirstName(),
				newManager.getLastName(), passwordEncoderService.encodePassword(newManager.getPassword()),listRole,newManager.getBaseSalary());
		rolesService.addNewRollManager(managerRepo.save(manager));
		return manager;
	}

	@Override
	@Transactional
	public void modifyManagerRole(Manager manager, List<Role> newRoleList) {
		List<Role> oldRoleList = manager.getRoles().stream()
	                                 .map(Roles::getRole)
	                                 .collect(Collectors.toList());
		List<Roles> oldRolesList = manager.getRoles();
		oldRolesList.forEach(oldRoles-> rolesService.upgrade(oldRoles,newRoleList.contains(oldRoles.getRole())));
		newRoleList.stream().filter(newRole-> !oldRoleList.contains(newRole)).forEach(newRole-> rolesService.addNewRoles(manager,newRole));
	}

	

}
