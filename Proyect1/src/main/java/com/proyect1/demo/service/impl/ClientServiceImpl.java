package com.proyect1.demo.service.impl;

import java.time.LocalDateTime;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyect1.demo.dto.ClientDTO;
import com.proyect1.demo.dto.GeneralInvoiceDTO;
import com.proyect1.demo.entity.Address;
import com.proyect1.demo.entity.Client;
import com.proyect1.demo.exception.PreConditionFailedException;
import com.proyect1.demo.exception.ResourceNotFoundException;
import com.proyect1.demo.repository.ClientRepository;
import com.proyect1.demo.repository.GeneralInvoiceRepository;
import com.proyect1.demo.repository.UserRepository;
import com.proyect1.demo.request.NewClientRequest;
import com.proyect1.demo.request.UpdateAddress;
import com.proyect1.demo.request.UpdateCliente;
import com.proyect1.demo.security.PasswordEncoderService;
import com.proyect1.demo.service.ClientService;
import com.proyect1.demo.service.MapperEntityToDTO;
import com.proyect1.demo.service.RolesService;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository clientRepo;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private GeneralInvoiceRepository generalInvoiceRepo;
	@Autowired
	private PasswordEncoderService passwordEncoderService;
	@Autowired
	private RolesService rolesService;
	@Autowired
	private MapperEntityToDTO mapperEntityToDto;

	@Transactional
	@Override
	public Client createClient(NewClientRequest newClient) throws PreConditionFailedException {
		assertThatNickAndEmailAreNotInUse(newClient.getNick(), newClient.getEmail());
		Address address = new Address(newClient.getStreetAndHeight(), newClient.getDepartment(), newClient.getCity(),
				newClient.getProvince(), newClient.getCountry(), newClient.getPostalCode(), newClient.getNick());
		Client client = new Client(newClient.getEmail(), newClient.getNick(), newClient.getFirstName(),
				newClient.getLastName(), passwordEncoderService.encodePassword(newClient.getPassword()),
				newClient.getMobile(), newClient.getDateOfBirth(), address);
		rolesService.addNewRollClient(clientRepo.save(client));
		return client;
	}

	private void assertThatNickAndEmailAreNotInUse(String nick, String email) throws PreConditionFailedException {
		if (userRepo.existsByNick(nick)) {
			throw new PreConditionFailedException("That nick is in use.Please try with other nick");
		}
		if (userRepo.existsByEmail(email)) {
			throw new PreConditionFailedException(
					"That email is in use.Please try with other. If you are de owner of this email : " + email
							+ " we can send you an email to recover your old nick and password.Choose that option if you wants ");
		}
	}

	public Client findByNickAndDownDateIsNull(String nick) throws ResourceNotFoundException {
		Client client =  clientRepo.findByNickAndDownDateIsNull(nick)
				.orElseThrow(() -> new ResourceNotFoundException("Client not found for this nick :: " + nick));
		return client;
	}

	@Override
	public ClientDTO findClientDTOByNickAndDownDateIsNull(String nick) throws ResourceNotFoundException {
		Client client = findByNickAndDownDateIsNull(nick);
		return mapperEntityToDto.clientToClientDTO(client);
	}

	@Transactional
	@Override
	public void updateClientFindByNickAndDateDownNotNull(String nick, @Valid UpdateCliente updateCliente)
			throws ResourceNotFoundException {

		Client client = findByNickAndDownDateIsNull(nick);
		client.setFirstName(updateCliente.getFirstName());
		client.setLastName(updateCliente.getLastName());
		client.setMobile(updateCliente.getMobile());
		client.setDateOfBirth(updateCliente.getDateOfBirth());
		client.getAddress().setStreetAndHeight(updateCliente.getStreetAndHeight());
		client.getAddress().setDepartment(updateCliente.getDepartment());
		client.getAddress().setCity(updateCliente.getCity());
		client.getAddress().setProvince(updateCliente.getProvince());
		client.getAddress().setCountry(updateCliente.getCountry());
		client.getAddress().setPostalCode(updateCliente.getPostalCode());
		clientRepo.save(client);
	}

	@Override
	public void setClientDownDateNowFindByNickAndDateDownNotNull(String nick) throws ResourceNotFoundException {
		Client client = findByNickAndDownDateIsNull(nick);
		client.setDownDate(LocalDateTime.now());

		clientRepo.save(client);
	}

	@Transactional
	@Override
	public void updateClientAddressFindByNickAndDateDownNotNull(String nick, @Valid UpdateAddress updateAddress)
			throws ResourceNotFoundException {
		Client client = findByNickAndDownDateIsNull(nick);
		client.getAddress().setStreetAndHeight(updateAddress.getStreetAndHeight());
		client.getAddress().setDepartment(updateAddress.getDepartment());
		client.getAddress().setCity(updateAddress.getCity());
		client.getAddress().setProvince(updateAddress.getProvince());
		client.getAddress().setCountry(updateAddress.getCountry());
		client.getAddress().setPostalCode(updateAddress.getPostalCode());
		clientRepo.save(client);
	}

	@Transactional
	@Override
	public void updateClientPasswordFindByNickAndDateDownNotNull(String nick, @Valid String password)
			throws ResourceNotFoundException {
		Client client = findByNickAndDownDateIsNull(nick);
		client.setPassword(password);
		clientRepo.save(client);
	}

	

	@Override
	public Page<GeneralInvoiceDTO>  find10GeneralInvoiceAndDownDateIsNull(String nick,Integer pageNumber) throws ResourceNotFoundException {
		Pageable pageOf10Items = PageRequest.of(pageNumber, 10);
		Page<GeneralInvoiceDTO>  generalInvoiceList = generalInvoiceRepo.findByClientNickOrderByCreationDateDesc(nick,pageOf10Items);
		return generalInvoiceList;
	}

}