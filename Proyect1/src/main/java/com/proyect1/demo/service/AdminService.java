package com.proyect1.demo.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.transaction.annotation.Transactional;

import com.proyect1.demo.entity.Manager;
import com.proyect1.demo.enums.Role;
import com.proyect1.demo.request.NewManagerRequest;

public interface AdminService {

	@Transactional
	Manager createManager(@Valid NewManagerRequest newManager, Role role);

	@Transactional
	void modifyManagerRole(Manager manager, List<Role> roles);

}
