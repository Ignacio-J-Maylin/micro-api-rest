package com.proyect1.demo.service;


import com.proyect1.demo.dto.ClientDTO;
import com.proyect1.demo.entity.Client;

public interface MapperEntityToDTO {
	

	ClientDTO clientToClientDTO(Client client);

}