package com.proyect1.demo.service.impl;



import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyect1.demo.entity.Manager;
import com.proyect1.demo.enums.Role;
import com.proyect1.demo.exception.PreConditionFailedException;
import com.proyect1.demo.exception.ResourceNotFoundException;
import com.proyect1.demo.repository.GeneralInvoiceRepository;
import com.proyect1.demo.repository.ManagerRepository;
import com.proyect1.demo.repository.UserRepository;
import com.proyect1.demo.request.NewManagerAdminRequest;
import com.proyect1.demo.request.NewManagerRequest;
import com.proyect1.demo.security.PasswordEncoderService;
import com.proyect1.demo.service.ManagerService;
import com.proyect1.demo.service.MapperEntityToDTO;
import com.proyect1.demo.service.RolesService;

@Service
public class ManagerServiceImpl implements ManagerService {

	@Autowired
	private ManagerRepository managerRepo;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private GeneralInvoiceRepository generalInvoiceRepo;
	@Autowired
	private PasswordEncoderService passwordEncoderService;
	@Autowired
	private RolesService rolesService;
	@Autowired
	private MapperEntityToDTO mapperEntityToDto;
	

	@Transactional
	@Override
	public Manager createManager(@Valid NewManagerRequest newManager) throws PreConditionFailedException {
		assertThatNickAndEmailAreNotInUse(newManager.getNick(), newManager.getEmail());
		Manager manager = new Manager(newManager.getEmail(), newManager.getNick(), newManager.getFirstName(),
				newManager.getLastName(), passwordEncoderService.encodePassword(newManager.getPassword()),newManager.getRoles(),(double) 0);
		rolesService.addNewRollManager(managerRepo.save(manager));
		return manager;
	}
	
	@Transactional
	@Override
	public Manager createManagerAdmin(@Valid NewManagerAdminRequest newAdmin) throws PreConditionFailedException {
		assertThatValidCode(newAdmin.getValidCode());
		assertThatNickAndEmailAreNotInUse(newAdmin.getNick(), newAdmin.getEmail());
		List<Role> adminRoles= getAdminRoles();
		Manager admin = new Manager(newAdmin.getEmail(), newAdmin.getNick(), newAdmin.getFirstName(),
				newAdmin.getLastName(), passwordEncoderService.encodePassword(newAdmin.getPassword()),adminRoles,(double) 0);
		rolesService.addNewRollManager(managerRepo.save(admin));
		return admin;
	}
	
	
	private List<Role> getAdminRoles() {
		List<Role> adminRoles= new ArrayList<Role>();
		adminRoles.add(Role.ROLE_ADMIN);
		adminRoles.add(Role.ROLE_ACCOUNTANT);
		adminRoles.add(Role.ROLE_CHEFF);
		adminRoles.add(Role.ROLE_LOGISTIC);
		adminRoles.add(Role.ROLE_SELLER);
		adminRoles.add(Role.ROLE_MANAGER);
		return adminRoles;
	}

	private void assertThatValidCode(String validCode) throws PreConditionFailedException {
		if (!"Qwerty123456".equals(validCode)) {
			throw new PreConditionFailedException("That validCode to create a manager is invalid. "
					+ "If you try wrong again, we are going to block your ip for our server.");
			
		}
	}


	private void assertThatNickAndEmailAreNotInUse(String nick, String email) throws PreConditionFailedException {
		if (userRepo.existsByNick(nick)) {
			throw new PreConditionFailedException("That nick is in use. Please try with other nick");
		}
		if (userRepo.existsByEmail(email)) {
			throw new PreConditionFailedException(
					"That email is in use.Please try with other. If you are de owner of this email : " + email
							+ " we can send you an email to recover your old nick and password.Choose that option if you wants ");
		}
	}

	@Override
	public Manager findByNickAndDateDownNull(String nick) throws ResourceNotFoundException {
		Manager manager =  managerRepo.findByNickAndDownDateIsNull(nick)
				.orElseThrow(() -> new ResourceNotFoundException("Client not found for this nick :: " + nick));
		return manager;
	}

}