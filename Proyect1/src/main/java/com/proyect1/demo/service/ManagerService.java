package com.proyect1.demo.service;

import javax.validation.Valid;

import org.springframework.transaction.annotation.Transactional;

import com.proyect1.demo.entity.Manager;
import com.proyect1.demo.exception.PreConditionFailedException;
import com.proyect1.demo.exception.ResourceNotFoundException;
import com.proyect1.demo.request.NewManagerAdminRequest;
import com.proyect1.demo.request.NewManagerRequest;

public interface ManagerService {

	@Transactional
	Manager createManager(@Valid NewManagerRequest newManager) throws PreConditionFailedException;

	@Transactional
	Manager createManagerAdmin(@Valid NewManagerAdminRequest newManager) throws PreConditionFailedException;

	Manager findByNickAndDateDownNull(String nick) throws ResourceNotFoundException;

	

	




}
