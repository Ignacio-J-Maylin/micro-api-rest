package com.proyect1.demo.service;


import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import com.proyect1.demo.dto.ClientDTO;
import com.proyect1.demo.dto.GeneralInvoiceDTO;
import com.proyect1.demo.entity.Client;
import com.proyect1.demo.exception.PreConditionFailedException;
import com.proyect1.demo.exception.ResourceNotFoundException;
import com.proyect1.demo.request.NewClientRequest;
import com.proyect1.demo.request.UpdateAddress;
import com.proyect1.demo.request.UpdateCliente;

public interface ClientService {

	
	@Transactional
	Client createClient(@Valid NewClientRequest nuevoClient) throws PreConditionFailedException;

	ClientDTO findClientDTOByNickAndDownDateIsNull(String nick) throws ResourceNotFoundException;

	@Transactional
	void updateClientFindByNickAndDateDownNotNull(String nick, @Valid UpdateCliente newClientDetails) throws ResourceNotFoundException;

	@Transactional
	void setClientDownDateNowFindByNickAndDateDownNotNull(String nick) throws ResourceNotFoundException;

	@Transactional
	void updateClientAddressFindByNickAndDateDownNotNull(String nick, @Valid UpdateAddress updateAddress) throws ResourceNotFoundException;

	@Transactional
	void updateClientPasswordFindByNickAndDateDownNotNull(String nick, @Valid String password) throws ResourceNotFoundException;
	

	@Transactional
	Page<GeneralInvoiceDTO>  find10GeneralInvoiceAndDownDateIsNull(String nick,Integer pageNumber) throws ResourceNotFoundException;

	




}
