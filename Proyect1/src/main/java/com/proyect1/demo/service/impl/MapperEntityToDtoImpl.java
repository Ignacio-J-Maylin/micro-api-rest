package com.proyect1.demo.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.proyect1.demo.dto.ClientDTO;
import com.proyect1.demo.dto.GeneralInvoiceDTO;
import com.proyect1.demo.entity.Client;
import com.proyect1.demo.entity.GeneralInvoice;
import com.proyect1.demo.service.MapperEntityToDTO;

@Service
public class MapperEntityToDtoImpl implements MapperEntityToDTO {

	@Override
	public ClientDTO clientToClientDTO(Client client) {
		if (client == null) {
			return null;
		}
		LocalDate today = LocalDate.now();
		LocalDate dayOfBirth =  LocalDate.of(today.getYear(), client.getDateOfBirth().getMonthValue(),client.getDateOfBirth().getDayOfMonth());
		if (dayOfBirth.isBefore(today)) {
			dayOfBirth = dayOfBirth.plusYears(1);
		}
		ClientDTO clientDTO = new ClientDTO();
		clientDTO.setAddress(client.getAddress().toString());
		clientDTO.setAge(String.valueOf(ChronoUnit.YEARS.between(client.getDateOfBirth(), LocalDate.now())));
		clientDTO.setBirthDay(dayOfBirth.format(DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy")));
		if(client.getLastName()==null || client.getLastName().isBlank() ||client.getFirstName()==null || client.getFirstName().isBlank())
		{
			clientDTO.setFullName(client.getNick());
		}
		else {
			clientDTO.setFullName(
					client.getLastName().substring(0, 1).toUpperCase() +client.getLastName().substring(1)+" "
					+client.getFirstName().substring(0, 1).toUpperCase() + client.getFirstName().substring(1));
			
		}
		clientDTO.setMobile(client.getMobile());
		clientDTO.setPoints(client.getPoints());
		return clientDTO;
	}

	public List<GeneralInvoiceDTO> listGeneralInvoiceToListGeneralInvoiceDTO(List<GeneralInvoice> listOfGeneralInvoice) {
		return listOfGeneralInvoice.stream().filter(Objects:: nonNull).map(this::generalInvoiceToGeneralInvoiceDTO).collect(Collectors.toList());
	}
	
	public GeneralInvoiceDTO generalInvoiceToGeneralInvoiceDTO(GeneralInvoice generalInvoice) {
		return new GeneralInvoiceDTO(generalInvoice);
	}


}
