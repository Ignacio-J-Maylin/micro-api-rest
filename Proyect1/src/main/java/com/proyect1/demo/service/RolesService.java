package com.proyect1.demo.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.proyect1.demo.entity.Client;
import com.proyect1.demo.entity.Manager;
import com.proyect1.demo.entity.Roles;
import com.proyect1.demo.enums.Role;

public interface RolesService {

	@Transactional
	List<Roles> addNewRollClient(Client clientCreated);
	
	@Transactional
	void addNewRollManager(Manager managerCreated);

	void inactivate(Roles oldRole);
	
	void activate(Roles oldRole);

	void addNewRoles(Manager manager, Role newRole);

	void upgrade(Roles oldRoles, boolean contains);
	

}
