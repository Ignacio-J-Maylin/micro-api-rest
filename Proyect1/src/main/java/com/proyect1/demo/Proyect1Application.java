package com.proyect1.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.proyect1.demo.security.JWTAuthorizationFilter;
import com.proyect1.demo.security.LoginService;

@SpringBootApplication
public class Proyect1Application extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(Proyect1Application.class, args);
	}

	
	

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Proyect1Application.class);
	}

	
	@EnableWebSecurity
	@Configuration
	@EnableGlobalMethodSecurity(prePostEnabled = true)
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Autowired
		private LoginService userDetailsService; 
	
		protected void setFilterChainProxySecurityConfigurer() {
			
		}
		
		

		/*
		 * Authorization filters and  configure entry points that you permit all user
		 * with out authorization
		 * */
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
				.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/login").permitAll()
			    .antMatchers(HttpMethod.POST, "/login/checkIn").permitAll()
			    .antMatchers(HttpMethod.GET, "/client/{clientId}").permitAll();
		}
		
		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.authenticationProvider(authProvider());
		}
		
		/*
		 * Here's a definition of beans in the Spring Framework documentation: In Spring,
		 *  the objects that form the backbone of your application and that are managed by
		 *   the Spring IoC container are called beans. A bean is an object that is 
		 *   instantiated, assembled, and otherwise managed by a Spring IoC container.
		 * */
		@Bean
		public DaoAuthenticationProvider authProvider() {
		    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		    authProvider.setUserDetailsService(userDetailsService);
		    authProvider.setPasswordEncoder(encoder());
		    return authProvider;
		}
		
		@Bean
		public PasswordEncoder encoder() {			
		    return new BCryptPasswordEncoder();
		}
		
	}
	
	@Configuration
	class CorsConfig implements WebMvcConfigurer {

		/*
		 * Configuring the cors conflict to permit node services consume this api.
		 * */
	    @Override
	    public void addCorsMappings(CorsRegistry registry) {

	        registry.addMapping("/**")
	                .allowedOrigins("http://localhost:4200")
	                .allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS", "TRACE", "PATCH")
	                .allowCredentials(true)
	        ;
	    }

	}
}
