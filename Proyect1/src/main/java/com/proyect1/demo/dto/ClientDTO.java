package com.proyect1.demo.dto;



public class ClientDTO {

	
	private String fullName;
	private String age;
	private Integer mobile;
	private String birthDay; 
	private Integer points;
	private String address;
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public Integer getMobile() {
		return mobile;
	}
	public void setMobile(Integer mobile) {
		this.mobile = mobile;
	}
	public String getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public ClientDTO(String fullName, String age, Integer mobile, String birthDay, Integer points, String address) {
		super();
		this.fullName = fullName;
		this.age = age;
		this.mobile = mobile;
		this.birthDay = birthDay;
		this.points = points;
		this.address = address;
	}
	public ClientDTO() {
	}
	
	
	
}
