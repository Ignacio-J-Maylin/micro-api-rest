package com.proyect1.demo.dto;

import java.util.ArrayList;
import java.util.List;

import com.proyect1.demo.enums.Role;


public class ManagerDTO {


	private String fullName;
	private Double baseSalary;
	private List<Role> roles = new ArrayList<Role>();
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Double getBaseSalary() {
		return baseSalary;
	}
	public void setBaseSalary(Double baseSalary) {
		this.baseSalary = baseSalary;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public ManagerDTO(String fullName, Double baseSalary, List<Role> roles) {
		super();
		this.fullName = fullName;
		this.baseSalary = baseSalary;
		this.roles = roles;
	}
	public ManagerDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
}
