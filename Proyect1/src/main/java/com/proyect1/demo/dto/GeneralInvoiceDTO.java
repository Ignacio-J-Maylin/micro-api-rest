package com.proyect1.demo.dto;

import java.time.LocalDateTime;

import com.proyect1.demo.entity.GeneralInvoice;

public class GeneralInvoiceDTO {

	private Long generalInvoiceId;
	private LocalDateTime creationDate;
	private LocalDateTime deliveryDate;
	private Double totalOrderPrice;
	private String deliveryPoint;
	private String orderStatus;
	private String paymentStatus;
	private String paymentMethod;

	public GeneralInvoiceDTO(Long generalInvoiceId, LocalDateTime creationDate, LocalDateTime deliveryDate,
			Double totalOrderPrice, String deliveryPoint, String orderStatus, String paymentStatus,
			String paymentMethod) {
		this.generalInvoiceId = generalInvoiceId;
		this.creationDate = creationDate;
		this.deliveryDate = deliveryDate;
		this.totalOrderPrice = totalOrderPrice;
		this.deliveryPoint = deliveryPoint;
		this.orderStatus = orderStatus;
		this.paymentStatus = paymentStatus;
		this.paymentMethod = paymentMethod;
	}

	public GeneralInvoiceDTO() {
	}

	public GeneralInvoiceDTO(GeneralInvoice generalInvoice) {
		this.generalInvoiceId = generalInvoice.getGeneralInvoiceId();
		this.creationDate = generalInvoice.getCreationDate();
		this.deliveryDate = generalInvoice.getDeliveryDate();
		this.totalOrderPrice = generalInvoice.getTotalOrderPrice();
		this.deliveryPoint = generalInvoice.getDeliveryPoint();
		this.orderStatus = generalInvoice.getOrderStatus().toString();
		this.paymentStatus = generalInvoice.getPaymentStatus().toString();
		this.paymentMethod = generalInvoice.getPaymentMethod().toString();
	}

	public Long getGeneralInvoiceId() {
		return generalInvoiceId;
	}

	public void setGeneralInvoiceId(Long generalInvoiceId) {
		this.generalInvoiceId = generalInvoiceId;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDateTime deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Double getTotalOrderPrice() {
		return totalOrderPrice;
	}

	public void setTotalOrderPrice(Double totalOrderPrice) {
		this.totalOrderPrice = totalOrderPrice;
	}

	public String getDeliveryPoint() {
		return deliveryPoint;
	}

	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	

}
