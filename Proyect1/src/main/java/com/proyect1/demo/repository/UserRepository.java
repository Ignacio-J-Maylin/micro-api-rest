package com.proyect1.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyect1.demo.entity.User;



@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	Optional<User> findOneByEmail(String email);

	Optional<User> findOneByNickAndPassword(String nick, String password);

	Optional<User> findOneByNick(String nick);

	boolean existsByNick(String nick);

	boolean existsByEmail(String email);

}