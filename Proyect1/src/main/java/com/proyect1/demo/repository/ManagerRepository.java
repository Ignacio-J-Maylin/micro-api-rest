package com.proyect1.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyect1.demo.entity.Manager;


@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long>{


	Optional<Manager> findByNick(String nick);

	Boolean existsByNick(String nick);

	Boolean existsByEmail(String email);

	Optional<Manager> findByNickAndDownDateIsNull(String nick);

}