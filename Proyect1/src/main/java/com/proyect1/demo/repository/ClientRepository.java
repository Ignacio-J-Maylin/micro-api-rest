package com.proyect1.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyect1.demo.entity.Client;


@Repository
public interface ClientRepository extends JpaRepository<Client, String>{


	Optional<Client> findByNick(String nick);

	Boolean existsByNick(String nick);

	Boolean existsByEmail(String email);

	Optional<Client> findByNickAndDownDateIsNull(String nick);

}