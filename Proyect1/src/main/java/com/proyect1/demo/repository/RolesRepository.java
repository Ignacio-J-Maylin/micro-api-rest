package com.proyect1.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyect1.demo.entity.Roles;


@Repository
public interface RolesRepository extends JpaRepository<Roles, Long>{

}