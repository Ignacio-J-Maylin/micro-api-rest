package com.proyect1.demo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyect1.demo.dto.GeneralInvoiceDTO;
import com.proyect1.demo.entity.GeneralInvoice;


@Repository
public interface GeneralInvoiceRepository extends JpaRepository<GeneralInvoice, Long>{

	Optional<GeneralInvoice> findByGeneralInvoiceId(Long GeneralInvoiceId);

	Page<GeneralInvoiceDTO> findByClientNickOrderByCreationDateDesc(String client, Pageable pageOf10Items);
}