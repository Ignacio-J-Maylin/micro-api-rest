package com.proyect1.demo.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;

import com.proyect1.demo.entity.User;
import com.proyect1.demo.exception.LockedException;
import com.proyect1.demo.exception.PreConditionFailedException;
import com.proyect1.demo.exception.ResourceNotFoundException;
import com.proyect1.demo.repository.UserRepository;
import com.proyect1.demo.request.LoginRequest;

import io.jsonwebtoken.SignatureAlgorithm;

import io.jsonwebtoken.Jwts;

@Service
public class LoginService implements UserDetailsService {


	@Autowired
	private UserRepository userRepo;

	public UserDetails loadUserByUsername(LoginRequest loginRequest) throws ResourceNotFoundException, LockedException, PreConditionFailedException {
		User user = userRepo.findOneByNick(loginRequest.getNick())
				.orElseThrow(() -> new ResourceNotFoundException("Nick not found with nick: "
						+ loginRequest.getNick() + "." + " Please try again."));
		

		return (UserDetails) user;
	}

	public String getJWTToken(UserDetails usuario) {
		String secretKey = JWTAuthorizationFilter.SECRET;
		List<String> roles = new ArrayList<String>();

		for (GrantedAuthority rol : usuario.getAuthorities()) {
			roles.add(rol.getAuthority());
		}
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList(getArrayToString(roles));

		String token = Jwts.builder().setId("softtekJWT").setSubject(usuario.getUsername())
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return JWTAuthorizationFilter.PREFIX + token;
	}

	public String getArrayToString(List<String> datos) {
		String texto = "";
		for (String dato : datos) {
			texto += dato + ",";
		}
		if (texto.length() > 1) {
			texto = texto.substring(0, texto.length() - 1);
		}
		return texto;
	}

	// not in use. Because, we search with nick and password. Not only userName
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepo.findOneByNick(username)
				.orElseThrow(() -> new UsernameNotFoundException("Nick not found with nick: "
						+ username + "." + " Please try again."));
	}
	
}