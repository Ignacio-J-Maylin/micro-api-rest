package com.proyect1.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordEncoderService {


	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	public String encodePassword(String password) {
		return passwordEncoder.encode(password);
	}

	public boolean checkPassword(String passowrd, String passwordEncode) {
		CharSequence passwordCharSequence = new StringBuffer(passowrd);
		return passwordEncoder.matches(passwordCharSequence, passwordEncode);

	}
}
