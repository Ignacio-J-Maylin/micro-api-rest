package com.proyect1.demo.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "USER")
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4173627236176094459L;

	@Column(name = "EMAIL", nullable = false, unique = true)
	protected String email;

	@Id
	@Column(name = "NICK", nullable = false, unique = true)
	protected String nick;

	@Column(name = "FIRST_NAME")
	protected String firstName;

	@Column(name = "LAST_NAME")
	protected String lastName;

	@Column(name = "PASSWORD")
	protected String password;

	@OneToMany(mappedBy = "user")
	private List<Roles> roles = new ArrayList<Roles>();

	@Column(name = "CREATED_DATE")
	protected LocalDateTime createdDate;

	@Column(name = "MODIFIED_DATE")
	protected LocalDateTime modifiedDate;

	@Column(name = "DOWN_DATE")
	protected LocalDateTime downDate;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Roles> getRoles() {
		return roles;
	}

	public void setRoles(List<Roles> roles) {
		this.roles = roles;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public LocalDateTime getDownDate() {
		return downDate;
	}

	public void setDownDate(LocalDateTime downDate) {
		this.downDate = downDate;
	}

	@Override
	public String toString() {
		return "User [email=" + email + ", nick=" + nick + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", password=" + password + ", roles=" + roles + ", createdDate=" + createdDate + ", downDate="
				+ downDate + "]";
	}

	public User(String email, String nick, String firstName, String lastName, String password) {
		this.email = email;
		this.nick = nick;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdDate = LocalDateTime.now();
		this.modifiedDate = null;
		this.downDate = null;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return	this.roles.stream()
		.filter(role-> role.getAvailable())
		.collect(Collectors.toList());
	}

	@Override
	public String getUsername() {
		return this.nick;
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.downDate.equals(null);
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.downDate.equals(null);
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return this.downDate.equals(null);
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public User() {
		super();
	}

}
