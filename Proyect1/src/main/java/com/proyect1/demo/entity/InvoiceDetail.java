package com.proyect1.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "INVOICE_DETAIL")
public class InvoiceDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INVOICE_DETAIL_ID")
	private Long invoiceDetailId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GENERAL_INVOICE_ID")
	private GeneralInvoice generalInvoice;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_ID")
	private Product product;

	@Column(name = "PRODUCT_NAME")
	private String productName;

	@Column(name = "BONUS_OR_SURCHARGE")
	private Double bonusOrSurcharge;

	@Column(name = "AMOUNT")
	private Integer amount;

	@Column(name = "OBSERVATION")
	private String observation;

	@Column(name = "FINAL_PRICE")
	private Double finalPrice;

	@Column(name = "UNIT_PRICE")
	private Double unitPrice;

	@Column(name = "CREATED_DATE")
	private LocalDateTime createdDate;

	@Column(name = "MODIFIED_DATE")
	private LocalDateTime modifiedDate;

	@Column(name = "DOWN_DATE")
	private LocalDateTime downDate;

	public InvoiceDetail(GeneralInvoice generalInvoice, Product product, Double bonusOrSurcharge, Integer amount,
			String observation) {
		super();
		this.generalInvoice = generalInvoice;
		this.product = product;
		this.productName = product.getName();
		this.bonusOrSurcharge = bonusOrSurcharge;
		this.amount = amount;
		this.observation = observation;
		this.finalPrice = product.getPrice() * amount * bonusOrSurcharge;
		this.unitPrice = product.getPrice();
		this.createdDate = LocalDateTime.now();
		this.modifiedDate = null;
		this.downDate = null;
	}

	@Override
	public String toString() {
		return "InvoiceDetail [invoiceDetailId=" + invoiceDetailId + ", generalInvoice="
				+ generalInvoice.getGeneralInvoiceId() + ", product=" + product.getProductId() + ", productName="
				+ productName + ", bonusOrSurcharge=" + bonusOrSurcharge + ", amount=" + amount + ", observation="
				+ observation + ", finalPrice=" + finalPrice + ", unitPrice=" + unitPrice + "]";
	}

	public Long getInvoiceDetailId() {
		return invoiceDetailId;
	}

	public void setInvoiceDetailId(Long invoiceDetailId) {
		this.invoiceDetailId = invoiceDetailId;
	}

	public GeneralInvoice getGeneralInvoice() {
		return generalInvoice;
	}

	public void setGeneralInvoice(GeneralInvoice generalInvoice) {
		this.generalInvoice = generalInvoice;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getBonusOrSurcharge() {
		return bonusOrSurcharge;
	}

	public void setBonusOrSurcharge(Double bonusOrSurcharge) {
		this.bonusOrSurcharge = bonusOrSurcharge;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public Double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public LocalDateTime getDownDate() {
		return downDate;
	}

	public void setDownDate(LocalDateTime downDate) {
		this.downDate = downDate;
	}
}
