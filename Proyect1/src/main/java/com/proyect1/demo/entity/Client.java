package com.proyect1.demo.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.proyect1.demo.enums.Role;

@Entity
@Table(name = "CLIENT")
@PrimaryKeyJoinColumn(referencedColumnName = "NICK")
public class Client extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = -24665914378434354L;

	@Column(name = "MOBILE")
	private Integer mobile;

	@Column(name = "DATE_OF_BIRTH")
	private LocalDate dateOfBirth;

	@Column(name = "POINTS")
	private Integer points;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "ADDRESS_ID")
	private Address address;

	@OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
	private List<GeneralInvoice> generalInvoice = new ArrayList<GeneralInvoice>();

	public Integer getMobile() {
		return mobile;
	}

	public void setMobile(Integer mobile) {
		this.mobile = mobile;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<GeneralInvoice> getGeneralInvoice() {
		return generalInvoice;
	}

	public void setGeneralInvoice(List<GeneralInvoice> generalInvoice) {
		this.generalInvoice = generalInvoice;
	}

	public Client(String email, String nick, String firstName, String lastName, String password, Integer mobile,
			LocalDate dateOfBirth, Address address) {
		super(email, nick, firstName, lastName, password);
		this.mobile = mobile;
		this.dateOfBirth = dateOfBirth;
		this.points = 0;
		this.address = address;
		List<Roles> roles = new ArrayList<Roles>();
		roles.add(new Roles(this, Role.ROLE_CLIENT));
		this.setRoles(roles);
	}

	public Client() {
		super();
	}

	@Override
	public String toString() {
		return "Client [mobile=" + mobile + ", dateOfBirth=" + dateOfBirth + ", points=" + points + ", address="
				+ address + ", generalInvoice=" + generalInvoice + "]";
	}

	

}
