package com.proyect1.demo.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.proyect1.demo.enums.OrderStatus;
import com.proyect1.demo.enums.PaymentMethod;
import com.proyect1.demo.enums.PaymentStatus;

@Entity
@Table(name = "GENERAL_INVOICE")
public class GeneralInvoice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "GENERAL_INVOICE_ID", nullable = false, unique = true)
	private Long generalInvoiceId;

	@Column(name = "CREATION_DATE")
	private LocalDateTime creationDate;

	@Column(name = "MODIFIED_DATE")
	private LocalDateTime modifiedDate;

	@Column(name = "REJECTED_DATE")
	private LocalDateTime rejectedDate;

	@Column(name = "DELIVERY_DATE")
	private LocalDateTime deliveryDate;

	@Column(name = "PAY_WITH")
	private Double payWith;

	@Column(name = "TOTAL_ORDER_PRICE")
	private Double totalOrderPrice;

	@Column(name = "DELIVERY_POINT")
	private String deliveryPoint;

	@Column(name = "ORDER_STATUS")
	@Enumerated(value = EnumType.STRING)
	private OrderStatus orderStatus;

	@Column(name = "PAYMENT_STATUS")
	@Enumerated(value = EnumType.STRING)
	private PaymentStatus paymentStatus;

	@Column(name = "PAYTMENT_METHOD")
	@Enumerated(value = EnumType.STRING)
	private PaymentMethod paymentMethod;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLIENT", referencedColumnName = "NICK")
	private Client client;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SELLER", referencedColumnName = "NICK")
	private Manager seller;

	@OneToMany(mappedBy = "generalInvoice", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<InvoiceDetail> invoiceDetailList = new ArrayList<InvoiceDetail>();

	public Long getGeneralInvoiceId() {
		return generalInvoiceId;
	}

	public void setGeneralInvoiceId(Long generalInvoiceId) {
		this.generalInvoiceId = generalInvoiceId;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public LocalDateTime getRejectedDate() {
		return rejectedDate;
	}

	public void setRejectedDate(LocalDateTime rejectedDate) {
		this.rejectedDate = rejectedDate;
	}

	public LocalDateTime getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDateTime deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Double getPayWith() {
		return payWith;
	}

	public void setPayWith(Double payWith) {
		this.payWith = payWith;
	}

	public Double getTotalOrderPrice() {
		return totalOrderPrice;
	}

	public void setTotalOrderPrice(Double totalOrderPrice) {
		this.totalOrderPrice = totalOrderPrice;
	}

	public String getDeliveryPoint() {
		return deliveryPoint;
	}

	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Manager getSeller() {
		return seller;
	}

	public void setSeller(Manager seller) {
		this.seller = seller;
	}

	public List<InvoiceDetail> getInvoiceDetailList() {
		return invoiceDetailList;
	}

	public void setInvoiceDetailList(List<InvoiceDetail> invoiceDetailList) {
		this.invoiceDetailList = invoiceDetailList;
	}

	public GeneralInvoice(LocalDateTime deliveryDate, Double payWith, String deliveryPoint, Client client,
			Manager seller, List<InvoiceDetail> invoiceDetailList) {
		this.creationDate = LocalDateTime.now();
		this.modifiedDate = null;
		this.rejectedDate = null;
		this.deliveryDate = deliveryDate;
		this.payWith = payWith;
		this.totalOrderPrice = invoiceDetailList.stream().map(item -> item.getFinalPrice()).reduce((double) 0,
				(a, b) -> a + b);

		this.deliveryPoint = deliveryPoint;
		this.orderStatus = OrderStatus.OBSERVATION;
		this.paymentStatus = PaymentStatus.PENDING;
		this.paymentMethod = PaymentMethod.CASH;
		this.client = client;
		this.seller = seller;
		this.invoiceDetailList = invoiceDetailList;
	}

	@Override
	public String toString() {
		return "GeneralInvoice [generalInvoiceId=" + generalInvoiceId + ", creationDate=" + creationDate
				+ ", deliveryDate=" + deliveryDate + ", payWith=" + payWith + ", totalOrderPrice=" + totalOrderPrice
				+ ", deliveryPoint=" + deliveryPoint + ", orderStatus=" + orderStatus + ", paymentStatus="
				+ paymentStatus + ", paymentMethod=" + paymentMethod + ", client=" + client + ", seller=" + seller
				+ ", invoiceDetailList=" + invoiceDetailList + "]";
	}

}
