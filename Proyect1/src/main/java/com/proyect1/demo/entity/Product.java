package com.proyect1.demo.entity;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT")
public class Product {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="PRODUCT_ID")
private Long productId;

@Column(name="CATEGORY")
private String category;

@Column(name="SUBCATEGORY")
private String subcategory;

@Column(name="NAME")
private String name;

@Column(name="DESCRIPTION")
private String description;

@Column(name="PRICE")
private Double price;

@Column(name="AVAILABLE")
private Boolean available;

@Column(name="STOCK")
private Integer stock;

@Column(name = "CREATED_DATE")
private LocalDateTime createdDate;

@Column(name = "MODIFIED_DATE")
private LocalDateTime modifiedDate;

@Column(name = "DOWN_DATE")
private LocalDateTime downDate;

public Product(String category, String subcategory, String name, String description, Double price,
		Boolean available, Integer stock) {
	this.category = category;
	this.subcategory = subcategory;
	this.name = name;
	this.description = description;
	this.price = price;
	this.available = available;
	this.stock = stock;
	this.createdDate = LocalDateTime.now();
	this.modifiedDate = null;
	this.downDate = null;
}

public Long getProductId() {
	return productId;
}

public void setProductId(Long productId) {
	this.productId = productId;
}

public String getCategory() {
	return category;
}

public void setCategory(String category) {
	this.category = category;
}

public String getSubcategory() {
	return subcategory;
}

public void setSubcategory(String subcategory) {
	this.subcategory = subcategory;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public Double getPrice() {
	return price;
}

public void setPrice(Double price) {
	this.price = price;
}

public Boolean getAvailable() {
	return available;
}

public void setAvailable(Boolean available) {
	this.available = available;
}

public Integer getStock() {
	return stock;
}

public void setStock(Integer stock) {
	this.stock = stock;
}

public LocalDateTime getCreatedDate() {
	return createdDate;
}

public void setCreatedDate(LocalDateTime createdDate) {
	this.createdDate = createdDate;
}

public LocalDateTime getModifiedDate() {
	return modifiedDate;
}

public void setModifiedDate(LocalDateTime modifiedDate) {
	this.modifiedDate = modifiedDate;
}

public LocalDateTime getDownDate() {
	return downDate;
}

public void setDownDate(LocalDateTime downDate) {
	this.downDate = downDate;
}

@Override
public int hashCode() {
	return Objects.hash(available, category, description, name, price, productId, stock, subcategory);
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Product other = (Product) obj;
	return Objects.equals(available, other.available) && Objects.equals(category, other.category)
			&& Objects.equals(description, other.description) && Objects.equals(name, other.name)
			&& Objects.equals(price, other.price) && Objects.equals(productId, other.productId)
			&& Objects.equals(stock, other.stock) && Objects.equals(subcategory, other.subcategory);
}

@Override
public String toString() {
	return "Product [productId=" + productId + ", category=" + category + ", subcategory=" + subcategory + ", name="
			+ name + ", description=" + description + ", price=" + price + ", available=" + available + ", stock="
			+ stock + "]";
}







}
