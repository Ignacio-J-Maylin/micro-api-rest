package com.proyect1.demo.entity;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="ADDRESS")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ADDRESS_ID", nullable=false, unique=true)
	private Long addressId;
	
	@Column(name="STREET_AND_HEIGHT")
	private String streetAndHeight;

	@Column(name="DEPARTMENT")
	private String department;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="PROVINCE")
	private String province;
	
	@Column(name="COUNTRY")
	private String country;

	@Column(name="POSTAL_CODE")
	private String postalCode;

	@Column(name="USER_NICK")
	private String userNick;
	
	@Column(name = "CREATED_DATE")
	private LocalDateTime createdDate;

	@Column(name = "MODIFIED_DATE")
	private LocalDateTime modifiedDate;

	@Column(name = "DOWN_DATE")
	private LocalDateTime downDate;
	
	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getStreetAndHeight() {
		return streetAndHeight;
	}

	public void setStreetAndHeight(String streetAndHeight) {
		this.streetAndHeight = streetAndHeight;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getUserNick() {
		return userNick;
	}

	public void setUserNick(String userNick) {
		this.userNick = userNick;
	}
	
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public LocalDateTime getDownDate() {
		return downDate;
	}

	public void setDownDate(LocalDateTime downDate) {
		this.downDate = downDate;
	}
	@Override
	public int hashCode() {
		return Objects.hash(addressId, city, country, department, postalCode, province, streetAndHeight, userNick);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		return Objects.equals(addressId, other.addressId) && Objects.equals(city, other.city)
				&& Objects.equals(country, other.country) && Objects.equals(department, other.department)
				&& Objects.equals(postalCode, other.postalCode) && Objects.equals(province, other.province)
				&& Objects.equals(streetAndHeight, other.streetAndHeight) && Objects.equals(userNick, other.userNick);
	}

	

	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", streetAndHeight=" + streetAndHeight + ", department=" + department
				+ ", city=" + city + ", province=" + province + ", country=" + country + ", postalCode=" + postalCode
				+ ", userNick=" + userNick + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate
				+ ", downDate=" + downDate + "]";
	}

	public Address( String streetAndHeight, String department, String city, String province,
			String country, String postalCode, String userNick) {
		this.streetAndHeight = streetAndHeight;
		this.department = department;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode;
		this.userNick = userNick;
		this.createdDate = LocalDateTime.now();
		this.modifiedDate = null;
		this.downDate = null;
	}

	public Address() {
		super();
	}




	
	
}
