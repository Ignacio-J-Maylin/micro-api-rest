package com.proyect1.demo.entity;




import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.proyect1.demo.enums.Role;


@Entity
@Table(name="ROLES")
public class Roles implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ROLES_ID",nullable=false,unique=true)
	private Long rolesId;


	@ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="NICK", referencedColumnName = "NICK")
	private User user;
	
	@Column(name="ROLE")
	@Enumerated(value = EnumType.STRING)
	private Role role;
	
	@Column(name = "CREATED_DATE")
	private LocalDateTime createdDate;

	@Column(name = "MODIFIED_DATE")
	private LocalDateTime modifiedDate;

	@Column(name = "AVAIBLE")
	private Boolean available;
	
    public Roles(User user, Role role) {

    	this.user = user;
    	this.role = role;
		this.createdDate = LocalDateTime.now();
		this.modifiedDate = null;
		this.available = true;
	}

	public Roles() {
	}

	public String getUser() {
		return user.getNick();
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	@Override
    public String getAuthority() {
        return this.role.toString();
    }

    @Override
	public String toString() {
		return "Roles [rolesId=" + rolesId + ", user=" + user + ", role=" + role + "]";
	}
    
    
}