package com.proyect1.demo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.proyect1.demo.enums.Role;

@Entity
@Table(name = "MANAGER")
@PrimaryKeyJoinColumn(referencedColumnName = "NICK")
public class Manager extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4091226458702445553L;

	@Column(name = "BASE_SALARY")
	private Double baseSalary;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private List<Roles> roles = new ArrayList<Roles>();

	public Manager(String email, String nick, String firstName, String lastName, String password,List<Role> listRole,Double baseSalary) {
		super(email, nick, firstName, lastName, password);
		List<Roles> roles = new ArrayList<Roles>();
		listRole.forEach(r->roles.add(new Roles(this, r)));
		this.setRoles(roles);
		this.baseSalary=baseSalary;
	}

	public Manager() {
		super();
		// TODO Auto-generated constructor stub
	}

	


}
