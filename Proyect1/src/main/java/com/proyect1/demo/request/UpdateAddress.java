package com.proyect1.demo.request;

import javax.validation.constraints.NotNull;

public class UpdateAddress {

	@NotNull(message="The street and height could not be null")
	private String streetAndHeight;
	@NotNull(message="The department could not be null")
	private String department;
	@NotNull(message="The city could not be null")
	private String city;
	@NotNull(message="The province could not be null")
	private String province;
	@NotNull(message="The country could not be null")
	private String country;
	@NotNull(message="The postal code could not be null")
	private String postalCode;
	
	public String getStreetAndHeight() {
		return streetAndHeight;
	}
	public void setStreetAndHeight(String streetAndHeight) {
		this.streetAndHeight = streetAndHeight;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public UpdateAddress(String streetAndHeight, String department, String city, String province,
			String country, String postalCode) {
		this.streetAndHeight = streetAndHeight;
		this.department = department;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode;
	}
	
	

}
