package com.proyect1.demo.request;



import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class NewManagerAdminRequest {

	@Email(message = "Email is mandatory. These email don't look good.Try to write correctly")
	private String email;
	@Size(min=6, max=20,message = "Nick must have between 6 to 20 caracters")
	@NotBlank(message = "Nick is mandatory.It could not be null or blank")
	private String nick;
	private String firstName;
	private String lastName;
	@Pattern(regexp="^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",message="The password must contain minimum eight characters, at least one letter and one number")
	private String password;
	@Pattern(regexp="^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",message="That validCode to create a manager is invalid. "
			+ "If you try wrong again, we are going to block your ip for our server.")
	private String validCode;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getValidCode() {
		return validCode;
	}
	public void setValidCode(String validCode) {
		this.validCode = validCode;
	}
	public NewManagerAdminRequest(String email, String nick, String firstName, String lastName, String password, String validCode) {
		super();
		this.email = email;
		this.nick = nick;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.validCode = validCode;
	}
	
	

}
