package com.proyect1.demo.request;

import java.time.LocalDate;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

public class UpdateCliente {

	@NotNull(message="The first name could not be null")
	private String firstName;
	@NotNull(message="The last name could not be null")
	private String lastName;
	@NotNull(message="The mobile number must be only numbers and between 11-0000-0000 to 15-9999-9999")
	@Min(1100000000)
	@Max(1599999999)
	private Integer mobile;
	@Past(message="The date Of Birth must be a date in the past")
	private LocalDate dateOfBirth;
	@NotNull(message="The street and height could not be null")
	private String streetAndHeight;
	@NotNull(message="The department could not be null")
	private String department;
	@NotNull(message="The city could not be null")
	private String city;
	@NotNull(message="The province could not be null")
	private String province;
	@NotNull(message="The country could not be null")
	private String country;
	@NotNull(message="The postal code could not be null")
	private String postalCode;
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Integer getMobile() {
		return mobile;
	}
	public void setMobile(Integer mobile) {
		this.mobile = mobile;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getStreetAndHeight() {
		return streetAndHeight;
	}
	public void setStreetAndHeight(String streetAndHeight) {
		this.streetAndHeight = streetAndHeight;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public UpdateCliente(String firstName, String lastName, String password, Integer mobile,
			LocalDate dateOfBirth, String streetAndHeight, String department, String city, String province,
			String country, String postalCode) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobile = mobile;
		this.dateOfBirth = dateOfBirth;
		this.streetAndHeight = streetAndHeight;
		this.department = department;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode;
	}
	
	

}
