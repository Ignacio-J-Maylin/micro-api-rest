package com.proyect1.demo.request;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class NewClientRequest {

	@Email(message = "Email is mandatory. These email don't look good.Try to write correctly")
	private String email;


	@Size(min=6, max=20,message = "Nick must have between 6 to 20 caracters")
	@NotBlank(message = "Nick is mandatory.It could not be null or blank")
	private String nick;
	private String firstName;
	private String lastName;
	@Pattern(regexp="^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",message="The password must contain minimum eight characters, at least one letter and one number")
	private String password;
	private Integer mobile;
	@Past(message="The date Of Birth must be a date in the past")
	private LocalDate dateOfBirth;
	private String streetAndHeight;
	private String department;
	private String city;
	private String province;
	private String country;
	private String postalCode;
	
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getMobile() {
		return mobile;
	}
	public void setMobile(Integer mobile) {
		this.mobile = mobile;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getStreetAndHeight() {
		return streetAndHeight;
	}
	public void setStreetAndHeight(String streetAndHeight) {
		this.streetAndHeight = streetAndHeight;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public NewClientRequest(String email, String nick, String firstName, String lastName, String password, Integer mobile,
			LocalDate dateOfBirth, String streetAndHeight, String department, String city, String province,
			String country, String postalCode) {
		super();
		this.email = email;
		this.nick = nick;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.mobile = mobile;
		this.dateOfBirth = dateOfBirth;
		this.streetAndHeight = streetAndHeight;
		this.department = department;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode;
	}
	
	

}
