package com.proyect1.demo.request;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.proyect1.demo.enums.Role;

public class NewManagerRequest {

	@Email(message = "Email is mandatory. These email don't look good.Try to write correctly")
	private String email;
	@Size(min = 6, max = 20, message = "Nick must have between 6 to 20 caracters")
	@NotBlank(message = "Nick is mandatory.It could not be null or blank")
	private String nick;
	private String firstName;
	private String lastName;
	@Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", message = "The password must contain minimum eight characters, at least one letter and one number")
	private String password;
	private List<Role> roles;
	@NotNull(message = "The base salary could not be null")
	@Positive(message = "The base salary must be a positive number")
	private Double baseSalary;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Role> getRoles() {
		if (!this.roles.contains(Role.ROLE_MANAGER)) {
			this.roles.add(Role.ROLE_MANAGER);
		}
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(Double baseSalary) {
		this.baseSalary = baseSalary;
	}

	public NewManagerRequest(String email, String nick, String firstName, String lastName, String password,
			List<Role> roles) {
		super();
		this.email = email;
		this.nick = nick;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.roles = roles;
	}

}
