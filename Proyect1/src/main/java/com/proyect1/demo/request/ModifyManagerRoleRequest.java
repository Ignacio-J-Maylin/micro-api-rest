package com.proyect1.demo.request;

import java.util.List;

import com.proyect1.demo.enums.Role;

public class ModifyManagerRoleRequest {

	private String nick;
	private List<Role> roles;
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public ModifyManagerRoleRequest(String nick, List<Role> roles) {
		super();
		this.nick = nick;
		this.roles = roles;
	}
	public ModifyManagerRoleRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
