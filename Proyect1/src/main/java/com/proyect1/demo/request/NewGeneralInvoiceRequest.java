package com.proyect1.demo.request;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.proyect1.demo.enums.PaymentMethod;

public class NewGeneralInvoiceRequest {

	
	@FutureOrPresent(message="The date of the delivery must be in the future")
	private LocalDateTime deliveryDate;
	@Size(min=6, max=150)
	@NotBlank(message = "The delivery point is mandatory.It could not be null and must have between 8 to 150 caracters")
	private String deliveryPoint;
	@Positive(message = "The number with which you are going to pay must be positive")
	private Double payWith;
	private PaymentMethod paymentMethod;
	private List<@NotBlank(message = "The invoices details can't be null or blanck") NewInvoiceDetailRequest> invoiceDetailList = new ArrayList<NewInvoiceDetailRequest>();
	public LocalDateTime getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(LocalDateTime deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryPoint() {
		return deliveryPoint;
	}
	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}
	public Double getPayWith() {
		return payWith;
	}
	public void setPayWith(Double payWith) {
		this.payWith = payWith;
	}
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public List<NewInvoiceDetailRequest> getInvoiceDetailList() {
		return invoiceDetailList;
	}
	public void setInvoiceDetailList(List<NewInvoiceDetailRequest> invoiceDetailList) {
		this.invoiceDetailList = invoiceDetailList;
	}
	public NewGeneralInvoiceRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NewGeneralInvoiceRequest(
			@FutureOrPresent(message = "The date of the delivery must be in the future") LocalDateTime deliveryDate,
			@Size(min = 6, max = 150) @NotBlank(message = "The delivery point is mandatory.It could not be null and must have between 8 to 150 caracters") String deliveryPoint,
			@Positive(message = "The number with which you are going to pay must be positive") Double payWith,
			PaymentMethod paymentMethod,
			List<@NotBlank(message = "The invoices details can't be null or blanck") NewInvoiceDetailRequest> invoiceDetailList) {
		super();
		this.deliveryDate = deliveryDate;
		this.deliveryPoint = deliveryPoint;
		this.payWith = payWith;
		this.paymentMethod = paymentMethod;
		this.invoiceDetailList = invoiceDetailList;
	}

	
}
