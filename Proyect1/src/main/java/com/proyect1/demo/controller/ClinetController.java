package com.proyect1.demo.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyect1.demo.dto.ClientDTO;
import com.proyect1.demo.dto.GeneralInvoiceDTO;
import com.proyect1.demo.exception.ResourceNotFoundException;
import com.proyect1.demo.request.NewGeneralInvoiceRequest;
import com.proyect1.demo.request.UpdateAddress;
import com.proyect1.demo.request.UpdateCliente;
import com.proyect1.demo.service.ClientService;

@RestController
@PreAuthorize("hasRole('ROLE_CLIENT')")
@RequestMapping("/client")
public class ClinetController {
	@Autowired
	private ClientService clientService;

	@GetMapping("")
	public ResponseEntity<ClientDTO> getClient() throws ResourceNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return ResponseEntity.ok().body(clientService.findClientDTOByNickAndDownDateIsNull(auth.getName()));
	}

	@GetMapping("/generalsInvoices")
	public ResponseEntity<Page<GeneralInvoiceDTO> > getFirst10GeneralsInvoices()
			throws ResourceNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Page<GeneralInvoiceDTO> last10GeneralInvoice = clientService.find10GeneralInvoiceAndDownDateIsNull(auth.getName(),0);
		return ResponseEntity.ok().body(last10GeneralInvoice);
	}
	@PostMapping("/generalsInvoices")
	public ResponseEntity<Page<GeneralInvoiceDTO> > createGeneralInvoice(@Valid @RequestBody NewGeneralInvoiceRequest newGeneralInvoice)
			throws ResourceNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Page<GeneralInvoiceDTO> last10GeneralInvoice = clientService.find10GeneralInvoiceAndDownDateIsNull(auth.getName(),0);
		return ResponseEntity.ok().body(last10GeneralInvoice);
	}
	
	@GetMapping("/generalsInvoices/page/{pageNumber}")
	public ResponseEntity<Page<GeneralInvoiceDTO> > findByNickAndDateDownNotAndgetAllGeneralFactura
	(@PathVariable(value = "pageNumber") Integer pageNumber) throws ResourceNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Page<GeneralInvoiceDTO>  listGeneralInvoiceDTO = clientService.find10GeneralInvoiceAndDownDateIsNull(auth.getName(),pageNumber);
		return ResponseEntity.ok().body(listGeneralInvoiceDTO);
	}

	@PutMapping("")
	public ResponseEntity<Map<String, Boolean>> findByNickAndDateDownNotNullAndupdateClient(
			@Valid @RequestBody UpdateCliente updateCliente) throws ResourceNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		clientService.updateClientFindByNickAndDateDownNotNull(auth.getName(), updateCliente);
		Map<String, Boolean> response = new HashMap<>();
		response.put("ClientChanged", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}

	@PutMapping("/address")
	public ResponseEntity<Map<String, Boolean>> findByNickAndDateDownNotNullAndUpdateClientAddress(
			@Valid @RequestBody UpdateAddress updateAddress) throws ResourceNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		clientService.updateClientAddressFindByNickAndDateDownNotNull(auth.getName(), updateAddress);
		Map<String, Boolean> response = new HashMap<>();
		response.put("ClientChanged", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}

	@PatchMapping("/password")
	public ResponseEntity<Map<String, Boolean>> findByNickAndDateDownNotNullAndUpdateClientPassword(
			@Valid @RequestBody String password) throws ResourceNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		clientService.updateClientPasswordFindByNickAndDateDownNotNull(auth.getName(), password);
		Map<String, Boolean> response = new HashMap<>();
		response.put("PasswordChanged", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}

	@DeleteMapping("")
	public Map<String, Boolean> findByNickAndDateDownNotNullAndSetDownDateNow() throws ResourceNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		clientService.setClientDownDateNowFindByNickAndDateDownNotNull(auth.getName());
		Map<String, Boolean> response = new HashMap<>();
		response.put("UserDown", Boolean.TRUE);
		return response;
	}
}