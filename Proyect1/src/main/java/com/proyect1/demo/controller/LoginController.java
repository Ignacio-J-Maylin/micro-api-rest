package com.proyect1.demo.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyect1.demo.entity.Client;
import com.proyect1.demo.entity.Manager;
import com.proyect1.demo.entity.User;
import com.proyect1.demo.exception.LockedException;
import com.proyect1.demo.exception.PreConditionFailedException;
import com.proyect1.demo.exception.ResourceNotFoundException;
import com.proyect1.demo.request.LoginRequest;
import com.proyect1.demo.request.NewClientRequest;
import com.proyect1.demo.request.NewManagerAdminRequest;
import com.proyect1.demo.security.LoginService;
import com.proyect1.demo.security.PasswordEncoderService;
import com.proyect1.demo.service.ClientService;
import com.proyect1.demo.service.ManagerService;

@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private LoginService loginService;
	@Autowired
	private ClientService clientService;
	@Autowired
	private ManagerService managerService;
	@Autowired
	private PasswordEncoderService passwordEncoderService;

	@PostMapping("/singUp/client")
	public ResponseEntity<Map<String, String>> createCliente(@Valid @RequestBody NewClientRequest newClient)
			throws ResourceNotFoundException, PreConditionFailedException, LockedException {
		Client clientCreated = clientService.createClient(newClient);
		Map<String, String> response = new HashMap<>();
		response.put("Token", loginService.getJWTToken(clientCreated));
		return ResponseEntity.created(URI.create("/client")).body(response);
	}
	@PostMapping("/singUp/manager/admin")
	public ResponseEntity<Map<String, String>> createManagerAdmin(@Valid @RequestBody NewManagerAdminRequest newManager)
			throws ResourceNotFoundException, PreConditionFailedException, LockedException {
		Manager adminCreated = managerService.createManagerAdmin(newManager);
		Map<String, String> response = new HashMap<>();
		response.put("Token", loginService.getJWTToken(adminCreated));
		return ResponseEntity.created(URI.create("/manager")).body(response);
	}

	@PostMapping("")
	public ResponseEntity<Map<String, String>> LoginUser(@Valid @RequestBody LoginRequest loginRequest)
			throws ResourceNotFoundException, LockedException, PreConditionFailedException {
		UserDetails user = loginService.loadUserByUsername(loginRequest.getNick());
		if (((User) user).getDownDate() != null) {
			throw new LockedException("That client is locked because it was deleted. Maybe you can make contact with us"
					+ " and we can alow that count.");
		}
		if (!passwordEncoderService.checkPassword(loginRequest.getPassword(), user.getPassword())) {
			throw new PreConditionFailedException("The password don´t much.Did you write it good?" + ".Try again!");
		}
		List<String> roles = new ArrayList<String>();
		for (GrantedAuthority rol : user.getAuthorities()) {
			roles.add(rol.getAuthority());
		}Map<String, String> response = new HashMap<>();
		response.put("Token", loginService.getJWTToken(user));
		return ResponseEntity.ok().body(response);
	}
}
