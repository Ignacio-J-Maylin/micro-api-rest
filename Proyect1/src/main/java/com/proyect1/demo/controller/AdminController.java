package com.proyect1.demo.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyect1.demo.entity.Manager;
import com.proyect1.demo.enums.Role;
import com.proyect1.demo.exception.LockedException;
import com.proyect1.demo.exception.PreConditionFailedException;
import com.proyect1.demo.exception.ResourceNotFoundException;
import com.proyect1.demo.request.ModifyManagerRoleRequest;
import com.proyect1.demo.request.NewManagerRequest;
import com.proyect1.demo.service.AdminService;
import com.proyect1.demo.service.ManagerService;

@RestController
@PreAuthorize("hasRole('ROLE_MANAGER')")
@RequestMapping("/login")
public class AdminController {
	@Autowired
	private AdminService AdminService;
	@Autowired
	private ManagerService managerService;

@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/createManagerCount/seller")
public ResponseEntity<Map<String, String>> createManagerSeller(@Valid @RequestBody NewManagerRequest newManager)
		throws ResourceNotFoundException, PreConditionFailedException, LockedException {
	Manager managerSellerCreated = AdminService.createManager(newManager,Role.ROLE_SELLER);
	Map<String, String> response = new HashMap<>();
	response.put("Nick", managerSellerCreated.getNick());
	return ResponseEntity.created(URI.create("/login")).body(response);
}


@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/createManagerCount/logistic")
public ResponseEntity<Map<String, String>> createManagerLogistic(@Valid @RequestBody NewManagerRequest newManager)
		throws ResourceNotFoundException, PreConditionFailedException, LockedException {
	Manager managerLogisticCreated = AdminService.createManager(newManager,Role.ROLE_LOGISTIC);
	Map<String, String> response = new HashMap<>();
	response.put("Nick", managerLogisticCreated.getNick());
	return ResponseEntity.created(URI.create("/login")).body(response);
}


@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/createManagerCount/cheff")
public ResponseEntity<Map<String, String>> createManagerCheff(@Valid @RequestBody NewManagerRequest newManager)
		throws ResourceNotFoundException, PreConditionFailedException, LockedException {
	Manager managerCheffCreated = AdminService.createManager(newManager,Role.ROLE_CHEFF);
	Map<String, String> response = new HashMap<>();
	response.put("Nick", managerCheffCreated.getNick());
	return ResponseEntity.created(URI.create("/login")).body(response);
}



@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/createManagerCount/accountant")
public ResponseEntity<Map<String, String>> createManagerAccountant(@Valid @RequestBody NewManagerRequest newManager)
		throws ResourceNotFoundException, PreConditionFailedException, LockedException {
	Manager managerAccountantCreated = AdminService.createManager(newManager,Role.ROLE_ACCOUNTANT);
	Map<String, String> response = new HashMap<>();
	response.put("Nick", managerAccountantCreated.getNick());
	return ResponseEntity.created(URI.create("/login")).body(response);
}



@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/modifyManager/roles")
public ResponseEntity<Map<String, Boolean>> createManagerAccountant(@Valid @RequestBody ModifyManagerRoleRequest ModifyManagerRole)
		throws ResourceNotFoundException, PreConditionFailedException, LockedException {
	Manager manager  = managerService.findByNickAndDateDownNull(ModifyManagerRole.getNick());
	AdminService.modifyManagerRole(manager,ModifyManagerRole.getRoles());
	Map<String, Boolean> response = new HashMap<>();
	response.put("Modified", true);
	return ResponseEntity.ok().body(response);
}
}