# MicroApiRestServicesDemo

Demo of micro APIRest services
Implements:
Spring Boot version 2.5.3
ORM (Hibernate +JPA) .
@EnableWebSecurity@PreAuthorize Token
@RestController, ResponseEntity.
@Service @Repository@Entity@Mapper
@ControllerAdvice @ExceptionHandler 
IntegrationTest @SpringBootTest @TestPropertySource @Sql TestRestTemplate


Let ´s start! 

if you have docker:
1) (option A )

   using docker-compose.yam  with spring_container: image: proyecto1:1.1

        A)
         - open terminal in ->proyecto1 
         there you jave a docker-compose and a dockerFile
         first at all. build a images with the docker file 
          terminal: micro-api-rest\Proyecto1>  sudo docker build -t proyecto1:1.1 .

          terminal: sudo docker images
        (you can see between your images some like
       REPOSITORY               TAG                IMAGE ID       CREATED        SIZE
       proyecto1                1.1                c7d10a97c459   3 days ago     194MB
        )
        B)
         Now, we can build the docker compose. 
         terminal: micro-api-rest\Proyecto1>  sudo docker-compose up -d

        wait to finish the process
          terminal: sudo docker ps
          (you should see some like this 
          CONTAINER ID   IMAGE           COMMAND                  CREATED        STATUS PORTS                                               NAMES
          fde79c7bf6ee   proyecto1:1.1   "java -jar app.jar"      35 hours ago   Up 35 hours   0.0.0.0:8082->8082/tcp:::8082->8082/tcp   proyecto1_spring_container_1
         325fb995070a   mysql:5.7       "docker-entrypoint.s…"   35 hours ago   Up 35 hours   3306/tcp, 33060tcp                         proyecto1_mysql_container_1
 
         (if you wants open de docker-compose to see what does it works) )

1)(option B )   using docker-compose.yam  with spring_container: image: ignaciomaylin/proyecto1:1.1

    you only have to run: 
    terminal in Proyecto1: docker-compose up -d
     wait to finish the process
          terminal: sudo docker ps
          (you should see some like this 
          CONTAINER ID   IMAGE           COMMAND                  CREATED        STATUS PORTS                                               NAMES
          fde79c7bf6ee   ignaciomaylin/proyecto1:1.1   "java -jar app.jar"      35 hours ago   Up 35 hours   0.0.0.0:8082->8082/tcp:::8082->8082/tcp   proyecto1_spring_container_1
         325fb995070a   mysql:5.7       "docker-entrypoint.s…"   35 hours ago   Up 35 hours   3306/tcp, 33060tcp                         proyecto1_mysql_container_1



now, you have a network that communicates with your computer on port 8082. which points to the server of the project 1 image that you created, and also has a container of a mysql image which communicates with the spring container within the internal network.

   C) Open your postman to comunicates with the proyect
       in Proyect1> you can see a file named
       "MicroApiRestServicesDemo.postman_collection.json"
       Import it in your postman.

   1)  you can create a cliente.
---------------------------------------
   post: localhost:8082/login/checkIn
   body raw  Json:   
   {
    "email": "pedrito@gmail.com",
    "nick": "pedrito",
    "firstName": "pedrito",
    "lastName": "monteros",
    "password": "pedrito123",
    "mobile": 1500000000,
    "dateOfBirth": [2015,8,1],
    "streetAndHeight": "address 282",
    "department": "A 3",
    "city": "city",
    "province": "state or porvince",
    "country": "country",
    "postalCode": "postalCode"
    }

    the response is ResponseEntity.status.created (201) with uri and a token.
---------------------------------------

   2) now try make a login with that nick and password

     post: localhost:8082/login
   body raw  Json:   
   {
    "nick": "pedrito",
    "password": "pedrito123"
    }


    the response is a token.



---------------------------------------

   3) lets see your perfil 

post: localhost:8082/client
  headers :   
    |KEY        |  VALUE
    |Token      | copy and paste the token that give you when you make the login


    the response is your client entity.



